## Wallet Broker

#### Assumptions
- Must have Java 8, Maven and Docker installed to build and test

### Building and Automated Testing
- To build this project and create the Docker image, run:
`mvn clean install dockerfile:build`
- Regular junit tests (`*Test.java`) will run and do not require a Redis
server running.
    - Unit tests can be run without installing or building the Docker
    container by running: `mvn clean test`
- For now, to run integration tests (`*IT.java`), *you need a redis server
running*.
    - To run ITs and start/stop a Redis server container by hand (quick ref):
        - `docker run --name monstermeshredis --rm -p 6379:6379 -d redis`
        - Then run this project's unit *and* integ tests:
        `mvn clean verify -Dtest=*Test,*IT`
        - To stop the Redis container: `docker stop monstermeshredis`

#### Build a Cluster and Deploy to GKE
The building and deploying of a cluster is automated for all the projects
in the demo. Follow the directions in the `infrastructure` project under
`clusters/gke/README.md`

#### Deploy to GKE (manually)
Development can be done on wallet-broker and tested in a running cluster.

    # build, tag and register new image
    mvn clean install -Dtest=*IT,*Test dockerfile:build
    # instead of mvn dockerfile:build, can do: docker build . -t monstermesh/wallet-broker

    # now tag the docker image with something meaningful
    docker tag monstermesh/wallet-broker:latest gcr.io/mc-digital-native/wallet-broker:<tag-name>
    # (optional) clean out old docker images:
    #    docker images; docker image rm <image>

    # push the image(s) to the Google Container Registry (gcr)
    gcloud docker -- push gcr.io/mc-digital-native/wallet-broker
    # (optional) list the images in gcr for wallet-broker,
    gcloud container images list-tags gcr.io/mc-digital-native/wallet-broker

    # create default route rule before deploying new image (use "replace"
    #    instead of "create" if you've already created on in this cluster)
    istioctl create -f k8s/route-rule-default.yaml
    # view/verify route rule
    istioctl get routerule wallet-broker -o yaml -n staging

    # deploy the <tag-name> version of wallet-broker (replacing "image_tag" parameter)
    cat k8s/deployment.yaml | sigil 'image_tag=gcr.io/mc-digital-native/wallet-broker:<tag-name>' | kubectl apply -f -

(sigil is used for parameter replacement and can be found
[here](https://github.com/gliderlabs/sigil))

#### Istio Route Rules
For more information about Istio Route Rules and running multiple versions
of wallet-broker, see [Routing and Versioning](route-rules/README.md).
