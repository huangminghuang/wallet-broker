package com.objectcomputing.monstermesh.model;

import java.util.Collection;

public class Wallets {

    private String uid;
    private String transaction_id;
    private Collection<Wallet> wallets;

    public Wallets() {/* required for serialization */}

    public Wallets(String transaction_id, String uid, Collection<Wallet> wallets) {
        this.transaction_id = transaction_id;
        this.uid = uid;
        this.wallets = wallets;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Collection<Wallet> getWallets() {
        return wallets;
    }

    public void setWallets(Collection<Wallet> wallets) {
        this.wallets = wallets;
    }

    public int size() {
        if (wallets == null || wallets.isEmpty()) {
            return 0;
        } else {
            return wallets.size();
        }
    }

    @Override
    public String toString() {
        return "Wallets{" +
                "uid='" + uid + '\'' +
                "transaction_id='" + transaction_id + '\'' +
                ", wallets=" + wallets +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Wallets wallets1 = (Wallets) o;

        if (!uid.equals(wallets1.uid)) return false;
        if (!transaction_id.equals(wallets1.transaction_id)) return false;
        return wallets.equals(wallets1.wallets);
    }

    @Override
    public int hashCode() {
        int result = uid.hashCode();
        result = 31 * result + transaction_id.hashCode();
        result = 31 * result + wallets.hashCode();
        return result;
    }
}
