package com.objectcomputing.monstermesh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WalletBroker {

    public static void main(String[] args) {
        SpringApplication.run(WalletBroker.class, args);
    }
}
