package com.objectcomputing.monstermesh.service;

import com.objectcomputing.monstermesh.model.Wallet;
import com.objectcomputing.monstermesh.model.Wallets;
import com.objectcomputing.monstermesh.registry.IssuerRegistry;
import com.objectcomputing.monstermesh.registry.UserCache;
import com.uber.jaeger.Tracer;
import io.opentracing.ActiveSpan;
import io.opentracing.propagation.Format;
import io.opentracing.propagation.TextMapInjectAdapter;
import io.opentracing.tag.Tags;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.objectcomputing.monstermesh.registry.IssuerConfig.TXN_ID_PAT;
import static com.objectcomputing.monstermesh.registry.IssuerConfig.UID_PAT;

@Component
public class WalletBrokerServiceImpl implements WalletBrokerService {
    // TODO: make all log.info() log.debug()
    private static final Logger LOGGER = LoggerFactory.getLogger(WalletBrokerServiceImpl.class);

    @Autowired
    private UserCache userCache;
    @Autowired
    private IssuerRegistry registry;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private Tracer tracer;

    private ExecutorService executor = Executors.newFixedThreadPool(5);

    @Override
    public Wallets retrieveWallets(Map<String, String> headers, String txnId, String userId) {
        Collection<Wallet> wallets = new ArrayList<>();

        // Check if user's issuers are cached
        List<String> issuerAddresses = userCache.getIssuersForUser(userId);
        boolean cached = ! issuerAddresses.isEmpty();
        if (! cached) {
            // Not cached. Attempt to fetch from all issuers
            issuerAddresses = new ArrayList<>(registry.getIssuers().values());
        }

        // spin thu all issuer addresses creating spans, creating and collecting callables to exec simultaneously
        Collection<Callable<IssuerToWallets>> futures = new ArrayList<>();
        for (String issuerAddress : issuerAddresses) {
            String issuerURI = parseIssuerAddress(issuerAddress, userId, txnId);
            ActiveSpan parentSpan = tracer.activeSpan();
            if (parentSpan != null) {
                LOGGER.debug("Parent span type: " + parentSpan.getClass().getName());
                LOGGER.debug("Parent span: " + (parentSpan.context() != null ? parentSpan.context()
                                                                                         .toString() : "null parent context"));
            } else {
                LOGGER.debug("Parent span type: null parent span");
            }
            try (ActiveSpan span = tracer.buildSpan("getWallets").asChildOf(parentSpan).startActive()) {

                Tags.SPAN_KIND.set(span, Tags.SPAN_KIND_CLIENT);
                Tags.HTTP_METHOD.set(span, "GET");
                Tags.HTTP_URL.set(span, issuerURI);
                tracer.inject(span.context(), Format.Builtin.HTTP_HEADERS, new TextMapInjectAdapter(headers));

                HttpHeaders httpHeaders = new HttpHeaders();
                headers.forEach(httpHeaders::set);
                HttpEntity entity = new HttpEntity(httpHeaders);

                // create all callables to invoke all at once below
                futures.add(new WalletFetcher(restTemplate, entity, userId, issuerURI));

                span.log(Collections.unmodifiableMap(Stream.of(
                        new AbstractMap.SimpleEntry<>("event", "getWallets"),
                        new AbstractMap.SimpleEntry<>("issuer", issuerURI),
                        new AbstractMap.SimpleEntry<>("user", userId))
                                                           .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey,
                                                                                     AbstractMap.SimpleEntry::getValue))));
            } catch (HttpStatusCodeException e) {
                if (e.getStatusCode().value() == 404) {
                    LOGGER.warn("No wallets found for " + userId + " in issuer " + issuerURI);
                } else {
                    LOGGER.warn("Whoops! " + e.getLocalizedMessage());
                }
            } catch (RestClientException e) {
                LOGGER.warn("Whoops! " + e.getLocalizedMessage());
            }
        }

        // invoke all and collect results
        try {
            Collection<Future<IssuerToWallets>> results = executor.invokeAll(futures);

            for (Future<IssuerToWallets> userIssuerWallet : results) {
                if (userIssuerWallet.isDone()) {
                    IssuerToWallets issuerWallets = null;
                    try {
                        issuerWallets = userIssuerWallet.get();
                        if (issuerWallets != null && ! issuerWallets.wallets.isEmpty()) {
                            // cache issuer for user if wallets found and not already cached
                            if (! cached) {
                                userCache.cacheUserIssuer(userId, issuerWallets.issuer);
                            }
                            wallets.addAll(issuerWallets.wallets);
                        }
                    } catch (ExecutionException t) {
                        LOGGER.warn("Damn! " + t.getLocalizedMessage());
                        if (cached && issuerWallets != null) {
                            userCache.removeIssuerFromUser(userId, issuerWallets.issuer);
                        }
                    }
                }
            }
        } catch (InterruptedException e) {
            LOGGER.warn("Interrupted! " + e.getLocalizedMessage());
            Thread.interrupted();
        }

        return new Wallets(txnId, userId, wallets);
    }

    static final class WalletFetcher implements Callable<IssuerToWallets> {
        final RestTemplate restTemplate;
        final HttpEntity entity;
        final String userId;
        final String issuerURI;

        WalletFetcher(RestTemplate restTemplate, HttpEntity entity, String userId, String issuerURI) {
            this.restTemplate = restTemplate;
            this.entity = entity;
            this.userId = userId;
            this.issuerURI = issuerURI;
        }

        @Override
        public IssuerToWallets call() {

            LOGGER.info("Looking for user '" + userId + "' wallets in issuer service: " + issuerURI);

            Collection<Wallet> issuerWalletsForUser = new ArrayList<>();
            try {
                ResponseEntity<Wallets> response = restTemplate.exchange(issuerURI, HttpMethod.GET, entity, Wallets.class);
                Wallets wallets = response.getBody();

                // previous spring boot method does not take http headers to pass along
//            wallets = restTemplate.getForObject(issuerURI, Wallets.class);

                issuerWalletsForUser.addAll(wallets.getWallets());
                if (wallets.size() > 0) {
                    LOGGER.info("Found " + wallets.size() + " wallets for user '" + userId + "' in issuer service: " + issuerURI);
                }
            } catch (HttpStatusCodeException e) {
                if (e.getStatusCode().value() == 404) {
                    LOGGER.warn("No wallets found for " + userId + " in issuer " + issuerURI);
                } else {
                    LOGGER.warn("Whoops! " + e.getLocalizedMessage());
                }
            } catch (RestClientException e) {
                LOGGER.warn("Whoops! " + e.getLocalizedMessage());
            }
            return new IssuerToWallets(issuerURI, issuerWalletsForUser);
        }
    }

    static final class IssuerToWallets {
        final String issuer;
        final Collection<Wallet> wallets;

        IssuerToWallets(String issuer, Collection<Wallet> wallets) {
            this.issuer = issuer;
            this.wallets = wallets;
        }
    }

    private static String parseIssuerAddress(String issuerAddress, String userId, String txnId) {
        String issuerURI = issuerAddress.replace(TXN_ID_PAT, txnId);
        return issuerURI.replace(UID_PAT, userId);
    }
}
