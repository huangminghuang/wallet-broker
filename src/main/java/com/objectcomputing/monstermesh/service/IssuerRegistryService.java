package com.objectcomputing.monstermesh.service;

import java.util.Map;

public interface IssuerRegistryService {
    void addIssuerMapping(String issuerId, String address);

    String findIssuerMapping(String issuerId);

    String removeIssuerMapping(String issuerId);

    Map<String, String> getIssuers();

}
