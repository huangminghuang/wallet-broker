package com.objectcomputing.monstermesh.service;

import com.objectcomputing.monstermesh.registry.IssuerRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class IssuerRegistryServiceImpl implements IssuerRegistryService {

    @Autowired
    private IssuerRegistry registry;

    @Override
    public void addIssuerMapping(String issuerId, String address) {
        registry.addIssuer(issuerId, address);
    }

    @Override
    public String findIssuerMapping(String issuerId) {
        return registry.findIssuer(issuerId);
    }

    @Override
    public String removeIssuerMapping(String issuerId) {
        return registry.removeIssuer(issuerId);
    }

    @Override
    public Map<String, String> getIssuers() {
        return registry.getIssuers();
    }
}
