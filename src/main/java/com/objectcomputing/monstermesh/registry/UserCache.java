package com.objectcomputing.monstermesh.registry;

import java.util.List;

public interface UserCache {

    List<String> getIssuersForUser(String userId);

    void cacheUserIssuer(String userId, String issuerAddress);

    boolean removeIssuerFromUser(String userId, String issuerAddress);

    boolean removeUser(String userId);

    void clear();

    void clearIssuer(String issuerAddress);
}
