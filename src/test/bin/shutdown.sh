#!/usr/bin/env bash

# Shut down and remove (because they were launched with --rm) all docker containers
# and the network

# Set the redis network name - set in startup.sh too!
export REDIS_NETWORK=monstermeshredis

echo "Stop wallet-broker"
docker stop wallet_broker

echo "Stop issuer EP"
docker stop issuer_ep

echo "Stop redis - $REDIS_NETWORK"
docker stop $REDIS_NETWORK

echo "Stop wallet-ui"
docker stop wallet_ui

echo "Remove the common network"
docker network rm monsternet

unset REDIS_NETWORK
