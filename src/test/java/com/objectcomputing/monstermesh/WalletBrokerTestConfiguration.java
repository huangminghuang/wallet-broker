package com.objectcomputing.monstermesh;

import com.objectcomputing.monstermesh.service.WalletBrokerService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class WalletBrokerTestConfiguration {

    @Bean
    @Primary
    public WalletBrokerService walletBrokerService() {
        return Mockito.mock(WalletBrokerService.class);
    }
}
