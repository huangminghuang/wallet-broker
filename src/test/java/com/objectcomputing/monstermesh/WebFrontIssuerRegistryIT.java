package com.objectcomputing.monstermesh;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalletBroker.class)
@WebAppConfiguration
public class WebFrontIssuerRegistryIT {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void addIssuer() throws Exception {
        String issuerId = "mojo";
        String issuerAddress = "https://localhost:9098/issuer/mojo";

        mockMvc.perform(get(buildPath("/broker/issuer", issuerId, "find")))
               .andExpect(status().isOk())
               .andReturn();

        mockMvc.perform(post("/broker/issuer/register")
                        .param("issuerId", issuerId)
                        .param("issuerAddress", issuerAddress))
               .andExpect(status().isOk())
               .andReturn();

        mockMvc.perform(get(buildPath("/broker/issuer", issuerId, "find")))
                                   .andExpect(status().isOk())
                                   .andReturn();
    }

    @Test
    public void removeIssuer() throws Exception {
        String issuerId = "fred";
        String address = "https://localhost:8080/fred";

        mockMvc.perform(post("/broker/issuer/register")
                                .param("issuerId", issuerId)
                                .param("issuerAddress", address))
               .andExpect(status().isOk())
               .andReturn();

        mockMvc.perform(post(buildPath("/broker/issuer/deregister", issuerId))
        .param("issuerId", issuerId))
               .andExpect(status().isOk())
               .andReturn();
    }

    @Test
    public void addIssuerAgain() throws Exception {
        String issuerId = "mojodeu";
        String address = "https://localhost:8080/mojodeu";
        String path = buildPath("/broker/issuer", issuerId, "find");

        mockMvc.perform(post("/broker/issuer/register")
                                .param("issuerId", issuerId)
                                .param("issuerAddress", address))
               .andExpect(status().isOk())
               .andReturn();

        mockMvc.perform(get(path))
               .andExpect(status().isOk())
               .andExpect(content().string(address))
               .andReturn();
    }

    @Test
    public void testHealth() throws Exception {
        mockMvc.perform(get("/health"))
               .andExpect(status().isOk())
               .andExpect(content().string("{\"status\":\"UP\"}"))
               .andReturn();
    }

    static String buildPath(String... pEle) {
        return Arrays.stream(pEle).collect(Collectors.joining("/"));
    }

}
