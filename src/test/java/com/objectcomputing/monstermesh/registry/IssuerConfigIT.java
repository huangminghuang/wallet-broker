package com.objectcomputing.monstermesh.registry;

import com.objectcomputing.monstermesh.WalletBrokerConfiguration;
import com.objectcomputing.monstermesh.controller.WalletBrokerController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalletBrokerConfiguration.class)
public class IssuerConfigIT {
    private static final String FRED = "fred";
    private static final String JOE = "joe";
    private static final String WILMA = "wilma";

    private static final String ADDRESS1 = "https://dhost:1414/DinoIssuer";
    private static final String ADDRESS2 = "https://fhost:1313/FrankinIssuer";
    private static final String ADDRESS3 = "https://rhost:4545/RubleWay";

    @Before
    public void beforeClass() throws Exception {
        userCache.removeUser(FRED);
        userCache.removeUser(JOE);
        userCache.removeUser(WILMA);

        issuerRegistry.removeIssuer(ADDRESS1);
        issuerRegistry.removeIssuer(ADDRESS2);
        issuerRegistry.removeIssuer(ADDRESS3);
    }

    @Autowired
    private IssuerRegistry issuerRegistry;

    @Autowired
    private UserCache userCache;

    @Autowired
    private WalletBrokerController controller;

    @Test
    public void userIssuersManual() throws Exception {

        userCache.cacheUserIssuer(FRED, ADDRESS1);
        userCache.cacheUserIssuer(FRED, ADDRESS2);
        issuerRegistry.addIssuer(IssuerConfig.getIssuerName(ADDRESS1), ADDRESS1);
        issuerRegistry.addIssuer(IssuerConfig.getIssuerName(ADDRESS2), ADDRESS2);
        issuerRegistry.addIssuer(IssuerConfig.getIssuerName(ADDRESS3), ADDRESS3);

        assertEquals(Arrays.asList(ADDRESS1, ADDRESS2), userCache.getIssuersForUser(FRED));
        assertEquals(ADDRESS1, issuerRegistry.findIssuer(IssuerConfig.getIssuerName(ADDRESS1)));

        String issuer = issuerRegistry.removeIssuer(IssuerConfig.getIssuerName(ADDRESS1));
        assertEquals(null, issuerRegistry.findIssuer(IssuerConfig.getIssuerName(ADDRESS1)));

        userCache.removeIssuerFromUser(FRED, issuer);
        assertEquals(Collections.singletonList(ADDRESS2), userCache.getIssuersForUser(FRED));
    }
    @Test
    public void userIssuersAuto() throws Exception {

        userCache.cacheUserIssuer(FRED, ADDRESS1);
        userCache.cacheUserIssuer(FRED, ADDRESS2);
        userCache.cacheUserIssuer(JOE, ADDRESS2);
        issuerRegistry.addIssuer(IssuerConfig.getIssuerName(ADDRESS1), ADDRESS1);
        issuerRegistry.addIssuer(IssuerConfig.getIssuerName(ADDRESS2), ADDRESS2);
        issuerRegistry.addIssuer(IssuerConfig.getIssuerName(ADDRESS3), ADDRESS3);

        Set<String> updatedIssuers = new HashSet<>(Arrays.asList(ADDRESS1, ADDRESS3));

        Set<String> newIssuers = IssuerConfig.ConfigModifier.issuersChanged(new IssuerConfig.IssuerAdder(issuerRegistry, userCache),
                                                                             new IssuerConfig.IssuerRemover(issuerRegistry, userCache),
                                                                             new HashSet<>(Arrays.asList(ADDRESS1, ADDRESS2, ADDRESS3)),
                                                                             updatedIssuers);

        assertEquals(updatedIssuers, newIssuers);
        assertEquals(Collections.singletonList(ADDRESS1), userCache.getIssuersForUser(FRED));

        List<String> current = Arrays.asList(ADDRESS1, ADDRESS3);
        Collections.sort(current);
        List<String> old = new ArrayList<>(issuerRegistry.getIssuers().values());
        Collections.sort(old);
        assertEquals(current, old);

        assertEquals(Collections.emptyList(), userCache.getIssuersForUser(JOE));
    }
}
