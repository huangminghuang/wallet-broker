package com.objectcomputing.monstermesh.registry;

import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import java.io.FileReader;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;

import static org.junit.Assert.*;

public class IssuerConfigTest {

    @Test
    public void testGetIssuerNameHttp() throws Exception {
        assertEquals("issuer2",
                     IssuerConfig.getIssuerName("http://issuer2:8080/v1/wallets/transaction_id/{txnId}/uid/{UID}"));
    }

    @Test
    public void testGetIssuerNameHttps() throws Exception {
        assertEquals("issuer2",
                     IssuerConfig.getIssuerName("https://issuer2:8080/v1/wallets/transaction_id/<txnId>/uid/<UID>"));
    }

    @Test
    public void testReadConfigMapFile() throws Exception {
        List<String> issuers = new TestReaderFunction().apply(new FileReader("k8s/config.yaml"));

        assertNotNull(issuers);
        assertEquals(3, issuers.size());
        assertTrue("Expected issuer1", issuers.contains("http://issuer1:8080/v1/wallets/transaction_id/<txnId>/uid/<UID>"));
        assertTrue("Expected issuer2", issuers.contains("http://issuer2:8080/v1/wallets/transaction_id/<txnId>/uid/<UID>"));
        assertTrue("Expected issuer3", issuers.contains("http://issuer3:8080/v1/wallets/transaction_id/<txnId>/uid/<UID>"));
    }

    @Test
    public void testReadConfigMapRaw() throws Exception {
        List<String> issuers = new IssuerConfig.ReaderFunction().apply(new FileReader("target/test-classes/test-config-raw.yaml"));

        assertNotNull(issuers);
        assertEquals(3, issuers.size());
        assertTrue("Expected issuer11", issuers.contains("http://issuer11:8080/v1/wallets/transaction_id/<txnId>/uid/<UID>"));
        assertTrue("Expected issuer22", issuers.contains("http://issuer22:8080/v1/wallets/transaction_id/<txnId>/uid/<UID>"));
        assertTrue("Expected issuer33", issuers.contains("http://issuer33:8080/v1/wallets/transaction_id/<txnId>/uid/<UID>"));
    }

    @Test
    public void testIssuersChangedRemoveAllAddAll() throws Exception {

        Adder adder = new Adder();
        Remover remover = new Remover();
        Set<String> old = new HashSet<>(Arrays.asList("a", "b", "c"));
        Set<String> neew = new HashSet<>(Arrays.asList("d", "e", "f"));

        Set<String> result = IssuerConfig.ConfigModifier.issuersChanged(adder, remover, old, neew);

        assertEquals(neew, adder.toAdd);
        assertEquals(old, remover.toRemove);
        assertEquals(neew, result);
    }

    @Test
    public void testIssuersChangedRemoveTwoAddThree() throws Exception {

        Adder adder = new Adder();
        Remover remover = new Remover();
        Set<String> old = new HashSet<>(Arrays.asList("a", "b", "c"));
        Set<String> neew = new HashSet<>(Arrays.asList("a", "d", "e", "f"));

        Set<String> result = IssuerConfig.ConfigModifier.issuersChanged(adder, remover, old, neew);

        assertEquals(new HashSet<>(Arrays.asList("d", "e", "f")), adder.toAdd);
        assertEquals(new HashSet<>(Arrays.asList("b", "c")), remover.toRemove);
        assertEquals(neew, result);
    }

    @Test
    public void testIssuersChangedRemoveAllAddNone() throws Exception {

        Adder adder = new Adder();
        Remover remover = new Remover();
        Set<String> old = new HashSet<>(Arrays.asList("a", "b", "c"));
        Set<String> neew = Collections.emptySet();

        Set<String> result = IssuerConfig.ConfigModifier.issuersChanged(adder, remover, old, neew);

        assertEquals(neew, adder.toAdd);
        assertEquals(old, remover.toRemove);
        assertEquals(neew, result);
    }

    @Test
    public void testIssuersChangedRemoveNoneAddAll() throws Exception {

        Adder adder = new Adder();
        Remover remover = new Remover();
        Set<String> old = new HashSet<>(Arrays.asList("a", "b", "c"));
        Set<String> neew = new HashSet<>(Arrays.asList("a", "b", "c", "d", "e", "f"));

        Set<String> result = IssuerConfig.ConfigModifier.issuersChanged(adder, remover, old, neew);

        assertEquals(new HashSet<>(Arrays.asList("d", "e", "f")), adder.toAdd);
        assertEquals(Collections.emptySet(), remover.toRemove);
        assertEquals(neew, result);
    }

    @Test
    public void testConfigModifier() throws Exception {

        Adder adder = new Adder();
        Remover remover = new Remover();

        IssuerConfig.ConfigModifier configModifier = new IssuerConfig.ConfigModifier(adder, remover, new TestReaderFunction());

        configModifier.modifyConfig(new FileReader("k8s/config.yaml"));
        Set<String> expectedFirst = new HashSet<>(Arrays.asList("http://issuer1:8080/v1/wallets/transaction_id/<txnId>/uid/<UID>",
                                                                "http://issuer2:8080/v1/wallets/transaction_id/<txnId>/uid/<UID>",
                                                                "http://issuer3:8080/v1/wallets/transaction_id/<txnId>/uid/<UID>"));
        assertEquals(expectedFirst, configModifier.issuerSet);

        configModifier.modifyConfig(new FileReader("target/test-classes/test-config1.yaml"));
        Set<String> expectedSecond = new HashSet<>(Arrays.asList("http://issuer10:8080/v1/wallets/transaction_id/<txnId>/uid/<UID>",
                                                                "http://issuer2:8080/v1/wallets/transaction_id/<txnId>/uid/<UID>",
                                                                "http://issuer30:8080/v1/wallets/transaction_id/<txnId>/uid/<UID>"));
        assertEquals(expectedSecond, configModifier.issuerSet);
    }

    // ===========================================================================

    static class TestReaderFunction implements Function<FileReader, List<String>> {

        @Override
        public List<String> apply(FileReader yamlPath) {
            List<String> list = new ArrayList<>();
            Yaml yaml = new Yaml();
            Map<String, Object> configProps = yaml.load(yamlPath);
            if (configProps != null && ! configProps.isEmpty()) {
                Map<String, String> data = (Map<String, String>) configProps.get("data");
                if (data != null && ! data.isEmpty()) {
                    String issuers = data.get("issuers");
                    list.addAll(Arrays.asList(issuers.split("\\s")));
                }
            }
            return list;
        }
    }

    static class Adder implements Consumer<String> {
        Set<String> toAdd = new HashSet<>();

        @Override
        public void accept(String s) {
            toAdd.add(s);
        }
    }

    static class Remover implements Consumer<String> {
        Set<String> toRemove = new HashSet<>();

        @Override
        public void accept(String s) {
            toRemove.add(s);
        }
    }
}
