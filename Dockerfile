FROM openjdk:8-jdk-alpine
VOLUME /deployments/config
RUN apk --no-cache add curl
ADD target/wallet-broker-0.1.0.jar broker-app.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /broker-app.jar
